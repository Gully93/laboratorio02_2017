package it.unimi.di.sweng.lab02;

public class BowlingGame implements Bowling {
	
	
    private static final int MAX_ROLLS = 20;
	private int[] rolls= new int[20]; 
    private int currentRoll=0;
	@Override
	public void roll(int pins) {
		rolls[currentRoll++]=pins;

	}

	@Override
	public int score() {
		int score=0;
		for(int currentRoll=0; currentRoll<MAX_ROLLS;currentRoll++){
			if( checkSpareStrike(currentRoll)==2){
				score+=rolls[currentRoll+2];} else
			if(checkSpareStrike(currentRoll)==1){
				score+=(rolls[currentRoll+1]+rolls[currentRoll+2]);
			}
				
			score+=rolls[currentRoll];}
			return score;
		}
	
	private int checkSpareStrike(int currentRoll){
		if(rolls[currentRoll]==10&& currentRoll%2==0)
			return 1;
		else if(currentRoll<MAX_ROLLS-1 && (rolls[currentRoll]+rolls[currentRoll+1])==10&&((currentRoll+1)%2)==1)
			return 2;
		return 0;
	}


	}


